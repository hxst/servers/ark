echo "---------------------------- NEW RUN ----------------------------"
case $ARK_MAP in
    TheIsland)
        MAP="TheIsland"
        MAP_NAME="The Island"
    ;;
    TheCenter)
        MAP="TheCenter"
        MAP_NAME="The Center"
    ;;
    ScorchedEarth|ScorchedEarth_P)
        MAP="ScorchedEarth_P"
        MAP_NAME="Scorched Earth"
    ;;
    Ragnarok)
        MAP="Ragnarok"
        MAP_NAME="Ragnarok"
    ;;
    Aberration|Aberration_P)
        MAP="Aberration_P"
        MAP_NAME="Aberration"
    ;;
    Extinction)
        MAP="Extinction"
        MAP_NAME="Extinction"
    ;;
    Valguero|Valguero_P)
        MAP="Valguero_P"
        MAP_NAME="Valguero"
    ;;
    CrystalIsles)
        MAP="CrystalIsles"
        MAP_NAME="Crystal Isles"
    ;;
    Genesis)
        MAP="Genesis"
        MAP_NAME="Genesis 1"
    ;;
    Genesis2|Gen2)
        MAP="Gen2"
        MAP_NAME="Genesis 2"
    ;;
    LostIsland)
        MAP="LostIsland"
        MAP_NAME="Lost Island"
    ;;
    Fjordur)
        MAP="Fjordur"
        MAP_NAME="Fjordur"
    ;;
    *)
        echo -e "You didn't specify a map name."
        exit
    ;;
esac

SESSION_NAME="[AU] Oceania.gg PVP $MAP_NAME [Crossplay/5x/No Mods]"

echo "--------------------------------------------------"
echo "- Detected Environment Variables -----------------"
echo MAP:           $MAP
echo MAP_NAME:      $MAP_NAME
echo GAME_PORT:     $GAME_PORT
echo QUERY_PORT:    $QUERY_PORT
echo RCON_PORT:     $RCON_PORT
echo SAVE_DIR_NAME: $SAVE_DIR_NAME
echo SESSION_NAME:  $SESSION_NAME
echo CLUSTER_ID:    $CLUSTER_ID
echo "--------------------------------------------------"

# Do Update
/data/steam/steamcmd.sh +force_install_dir /data/game/ +login anonymous +app_update 376030 validate +quit && WEGUCCI=1 || WEGUCCI=0

# Copy custom configs over existing.
# I have to do this because wildcard are fucking dogshit and overwrite custom settings because they're fucking dogshit.
cp /data/game/ShooterGame/Saved/Config/LinuxServer/Game.ini.custom             /data/game/ShooterGame/Saved/Config/LinuxServer/Game.ini
cp /data/game/ShooterGame/Saved/Config/LinuxServer/GameUserSettings.ini.custom /data/game/ShooterGame/Saved/Config/LinuxServer/GameUserSettings.ini

# (Hopefully) Prevent the Ark from overwriting the config for no reason.
chown -R root:root /data/game/ShooterGame/Saved/Config/LinuxServer
chmod -R 755       /data/game/ShooterGame/Saved/Config/LinuxServer

echo "----------------------------------------------- ls"
ls -la /data/game/ShooterGame/Saved/Config/LinuxServer/

## Updating mods with steamcmd currently broken. Thanks, Wildcard!
#/data/steam/steamcmd.sh +login anonymous +force_install_dir /data/game/ +workshop_download_item 346110 1999447172 +quit
echo "-------------------------------------- Launch Game"
if [ "$WEGUCCI" == 1 ]
then
    su ark -c "/data/game/ShooterGame/Binaries/Linux/ShooterGameServer ${MAP}?listen?Port=${GAME_PORT}?QueryPort=${QUERY_PORT}?RCONEnabled=True?RCONPort=${RCON_PORT}?ServerAdminPassword=${RCON_PASSWORD}?AltSaveDirectoryName=${SAVE_DIR_NAME}?SessionName=\"${SESSION_NAME}\"?PreventDownloadSurvivors=False?PreventDownloadItems=False?PreventDownloadDinos=False?PreventUploadSurvivors=False?PreventUploadItems=False?PreventUploadDinos=False?ShowFloatingDamageText=false?PreventOfflinePvP=false?PreventOfflinePvPInterval=300?DisableImprintDinoBuff=false?AllowFlyingStaminaRecovery=true?PreventSpawnAnimations=false?AdminLogging=true -NoTransferFromFiltering -clusterid=${CLUSTER_ID} -server -crossplay -webalarm -EnableIdlePlayerKick -ForceRespawnDinos -servergamelog -UseItemDupeCheck"
else
    exit 1
fi
